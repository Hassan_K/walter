"use client"
import "../styles/globals.css"
import Navbar from "@/components/Navbar"
import Footer from "@/components/Footer"
import { ThemeProvider } from "next-themes"

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en" className="md:w-[100%] w-[105%] md:ml-0 ml-[1px]">
     
      <head />
  <body className="md:w-[100%] w-[26rem]" style={{
        background:"linear-gradient(201deg, rgba(84,66,65,1) 0%, rgba(206,192,173,1) 60%, rgba(206,192,173,1) 100%)" 
      }}>        <ThemeProvider enableSystem={true} attribute="class">
          <Navbar />
          {children}
          <br />
          <Footer />
        </ThemeProvider>
      </body>
    </html>
  )
}
