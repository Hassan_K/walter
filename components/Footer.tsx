"use client"
import { Icon } from "@iconify/react"
import React from "react"
import {
  AiOutlineGithub,
  AiOutlineTwitter,
  AiOutlineLinkedin,
  AiOutlineYoutube,
} from "react-icons/ai"
import "./button.css"

// className=' flex items-center justify-center text-center  border-4 border-[#fff] bg-[#544241] text-[#fff] w-[200px] md:w-[250px] rounded-full font-bold py-2 mb-4 mr-[50px] md:mr-[170px]' 
const Footer = () => {
  return (
    <footer className="mx-auto w-full px-4 sm:px-6  bg-[#4b3b3af7] md:mt-[130px] ">
      
     <br />

        <div className="flex flex-row items-center justify-center space-x-[6rem] ">
 <img className="hidden md:block" src="/bone.png" alt="" style={{width:"70px",height:"70px"}} />
          <a href="https://raydium.io/swap/?inputMint=sol&outputMint=FV56CmR7fhEyPkymKfmviKV48uPo51ti9kAxssQqTDLu" rel="noreferrer" target="_blank">

          <button className="button-85 ml-[-5rem] md:ml-0" role="button" >BUY HERE!</button>
          </a>
          <a
            href="https://x.com/WalterDogSolana?mx=2"
            rel="noreferrer"
            target="_blank"
          >
            <button className="button-851 md:w-[9rem] md:ml[-5rem] ml-[-4rem] " role="button"> <Icon icon="pajamas:twitter" className="x" width="52" height="52"  style={{color: "white"}} /> </button>

          </a>
 <img className="hidden md:block" src="/bone.png" alt="" style={{width:"70px",height:"70px"}} />
       
        </div>
<br />
    </footer>
  )
}

export default Footer
