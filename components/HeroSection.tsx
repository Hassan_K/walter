"use client" // this is a client component
import React, { CSSProperties, useState } from "react"
import Image from "next/image"
import { Link } from "react-scroll/modules"
import { HiArrowDown } from "react-icons/hi"
import { Icon } from '@iconify/react';
import AboutSection from "./AboutSection"
import { Text } from "./Text"

const HeroSection = () => {
  const [heroImage, setHeroImage] = useState('/aa.png');
  const [backgroundImage, setBackgroundImage] = useState(null);

  const [heroImageStyles, setHeroImageStyles] = useState({
    width: '400px',
    height: '400px',
    marginLeft: '-30px',
  });
 
const changeImage = (imgPath: string, styles: CSSProperties) => {
  console.log("Change image function called with path and styles:", imgPath, styles);

  // Ensure 'width', 'height', and 'marginLeft' are strings
  const newStyles = {
    width: styles.width ? styles.width.toString() : '', // Check if width is defined
    height: styles.height ? styles.height.toString() : '', // Check if height is defined
    marginLeft: styles.marginLeft ? styles.marginLeft.toString() : '', // Check if marginLeft is defined
  };

  // Set the hero image and styles
  setHeroImage(imgPath);
  setHeroImageStyles(newStyles);
};

const handleReset = () => {
  // Reset the hero image and styles
  setHeroImage('/aa.png');
  setHeroImageStyles({
    width: '400px',
    height: '400px',
    marginLeft: '-30px',
  });
  setBackgroundImage(null);

};

 const handleDownload = () => {
    // Create a virtual anchor element
    const link = document.createElement('a');
    link.href = heroImage; // Set the href attribute to the currently rendered image
    link.download = 'image.png'; // Set the download attribute with a default filename
    document.body.appendChild(link); // Append the anchor element to the document body
    link.click(); // Simulate a click event to trigger the download
    document.body.removeChild(link); // Remove the anchor element from the document body
  };

  return (
    <div className=' md:max-w-[1240px] mx-auto grid md:grid-cols-2 '>
  <div className=' ml-[18px] w-[300px] md:h-[300px] md:w-[290px] md:mr-[130px] relative flex justify-end  '>
  <div className="hidden md:block px-10 w-32 border-r-2 border-[#544241] md:mt-2 md:w-1/2 md:mr-[-450px] md:h-[900px]"></div>
  <img src="/paws.png" alt="" className="hidden md:block rotate-12 md:w-[95px]  md:h-[95px] md:mr-[40px] "/>
  <img src="/dogfood.png" alt="" className="hidden md:block rotate-6 z-[-1] md:w-[180px]  md:h-[160px] md:mt-[30rem] md:mr-[-14rem]"/>
        {backgroundImage && (
          <img
            className="absolute z-0"
            style={{ ...heroImageStyles, opacity: 0.5 }} // Adjust opacity if needed
            src={backgroundImage}
            alt="background"
          />
        )}
        <img
          className="w-[290px] md:mr-[55px] z-[100] mx-auto my-4 bg-[#2118151f] border-solid border-8 border-[#544241] rounded-xl ml-[10px]  md:h-[320px] md:mt-[30px] relative"
          src={heroImage}
          alt="hero"
        />

   <button onClick={handleReset} className='bg-[#ffffff5a] flex items-center justify-center text-center absolute bottom-[-60px] md:bottom-[-140px] border-4 border-[#544241]  text-[#544241] w-[200px] md:w-[250px] rounded-full font-bold py-2 mb-4 mr-[50px] md:mr-[77px] '  style={{ fontFamily: "'Bitter', sans-serif", fontSize: "18px"}}>
  <Icon icon="system-uicons:reset-forward" width="20" style={{ color: "#544241", marginRight: "1px"}} />
  Reset
</button>

<button onClick={handleDownload} className=' flex items-center justify-center text-center absolute bottom-[-120px] md:bottom-[-200px] border-4 border-[#fff] bg-[#544241] text-[#fff] w-[200px] md:w-[250px] rounded-full font-bold py-2 mb-4 mr-[50px] md:mr-[77px]'  style={{ fontFamily: "'Bitter', sans-serif", fontSize: "18px"}}>   
 <Icon icon="material-symbols:download" width="20" style={{ color: "#fff", marginRight: "1px"}} />
      Download</button>

      
  
  </div>
  
  <div className=" ml-[68px] md:ml-[-120px]  md:mr-2 md:relative md:left-2 relative left-[-70px] mt-[120px] md:mt-[30px] w-[340px] md:w-[600px]   text-[#4b3b3a]" style={ {fontFamily: "'Bitter', sans-serif"}}>
          
           <h1 className="md:text-sm md:ml-[-4px] text-4xl font-bold ml-[3rem] md:mt-0" style={{fontSize:'20px'}}>CREATE YOUR WALTER</h1>
           
       <div >
       
       <AboutSection changeImage={changeImage} /> 
      
       </div>
         </div>
</div>
  )
}

export default HeroSection
