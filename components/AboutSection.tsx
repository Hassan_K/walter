"use client"

import React, { CSSProperties } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import './AboutSection.css'; // Import CSS file for custom styling
import { Icon, InlineIcon } from '@iconify/react';


interface ArrowProps {
  onClick: () => void;
}
function PrevArrow(props: ArrowProps) {
  const { onClick } = props;
  return (
    <div className="slick-arrow hidden md:block  prev-arrow ml-[25px] md:ml-[6px] bg-[#ffffff3c] border-2 border-solid border-[#4b3b3a]" onClick={onClick}>
<Icon icon="fluent:ios-arrow-left-24-regular" width="42" height="42"  style={{color: "black" , cursor:"pointer"}} />    </div>
 
  );
}

function NextArrow(props: ArrowProps) {
  const { onClick } = props;
  return (
    <div className="slick-arrow hidden md:block next-arrow mr-[24px] md:mr-[-10px] bg-[#ffffff3c]  border-2 border-solid border-[#4b3b3a]" onClick={onClick}>
<Icon icon="fluent:ios-arrow-right-24-regular" width="42" height="42"  style={{color: "black",  marginLeft:"-18px",cursor:"pointer" }} /></div>
  );
}
interface DataItem {
  name: string;
  img: string;
}
interface ImageData {
  imgPath: string;
  width: number;
  height: number;
  marginLeft: string;
}

// Added type definitions for props passed to the AboutSection component
// Update AboutSectionProps interface
interface AboutSectionProps {
  changeImage: (imgPath: string, styles: CSSProperties) => void;
}

function AboutSection(props: AboutSectionProps) {
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 5, // Default number of slides to show
    slidesToScroll: 1,
    prevArrow: <PrevArrow onClick={() => {}} />, // Pass onClick prop to PrevArrow
    nextArrow: <NextArrow onClick={() => {}} />, // Pass onClick prop to NextArrow
    responsive: [
      {
        breakpoint: 768, // Breakpoint for mobile devices
        settings: {
          slidesToShow: 3, // Number of slides to show on mobile devices
        }
      }
    ]
  };
  
  const handleClick = (index: number) => {
    console.log("Slide clicked:", index);
    if (index === 1) {
      props.changeImage('/newht.png', { width: '400px', height: '400px', marginLeft: '-30px' });
    } 
    else if (index === 0) {
      props.changeImage('/aa.png', { width: '5px', height: '511px', marginLeft: '-5000px' });
    }
    else if (index === 2) {
      props.changeImage('/balnews.png', { width: '5px', height: '511px', marginLeft: '-5000px' });
    } else if (index === 3) {
      props.changeImage('/gnnew.png', { width: '600px', height: '600px', marginLeft: '-40px' });
    } else if (index === 4) {
      props.changeImage('/mfingnew.png', { width: '700px', height: '700px', marginLeft: '-60px' });
    } 
    else if (index === 5) {
      props.changeImage('/head/curl.png', { width: '800px', height: '800px', marginLeft: '-70px' });
    }
    else if (index === 6) {
      props.changeImage('/head/ric.png', { width: '800px', height: '800px', marginLeft: '-70px' });
    }
    else if (index === 7) {
      props.changeImage('/head/ma.png', { width: '800px', height: '800px', marginLeft: '-70px' });
    }
    else if (index === 8) {
      props.changeImage('/head/magic.png', { width: '800px', height: '800px', marginLeft: '-70px' });
    }
    else if (index === 9) {
      props.changeImage('/head/headph.png', { width: '800px', height: '800px', marginLeft: '-70px' });
    }
  };
  const handleClick2 = (index: number) => {
    console.log("Slide clicked:", index);
    if (index === 1) {
      props.changeImage('/head/redcap.png', { width: '400px', height: '400px', marginLeft: '-30px' });
    } 
    else if (index === 0) {
      props.changeImage('/aa.png', { width: '5px', height: '511px', marginLeft: '-5000px' });
    }
    else if (index === 2) {
      props.changeImage('/head/aussie.png', { width: '5px', height: '511px', marginLeft: '-5000px' });
    } else if (index === 3) {
      props.changeImage('/head/greenhair.png', { width: '600px', height: '600px', marginLeft: '-40px' });
    } else if (index === 4) {
      props.changeImage('/head/redwhitehair.png', { width: '700px', height: '700px', marginLeft: '-60px' });
    } else if (index === 5) {
      props.changeImage('/head/bluehair.png', { width: '800px', height: '800px', marginLeft: '-70px' });
    }
     else if (index === 6) {
      props.changeImage('/head/redhair.png', { width: '800px', height: '800px', marginLeft: '-70px' });
    }
     else if (index === 7) {
      props.changeImage('/head/blackspikes.png', { width: '800px', height: '800px', marginLeft: '-70px' });
    }
     else if (index === 8) {
      props.changeImage('/head/lbspikes.png', { width: '800px', height: '800px', marginLeft: '-70px' });
    }
    
     else if (index === 9) {
      props.changeImage('/head/redspikes.png', { width: '800px', height: '800px', marginLeft: '-70px' });
    }
     else if (index === 10) {
      props.changeImage('/head/pinkspikes.png', { width: '800px', height: '800px', marginLeft: '-70px' });
    }
     else if (index === 11) {
      props.changeImage('/head/firered.png', { width: '800px', height: '800px', marginLeft: '-70px' });
    }
     else if (index === 12) {
      props.changeImage('/head/bunny.png', { width: '800px', height: '800px', marginLeft: '-70px' });
    }
  };
  const handleClick3 = (index: number) => {
    console.log("Slide clicked:", index);
    if (index === 1) {
      props.changeImage('/head/face1_new.png', { width: '400px', height: '400px', marginLeft: '-30px' });
    } 
    else if (index === 0) {
      props.changeImage('/aa.png', { width: '5px', height: '511px', marginLeft: '-5000px' });
    }
    else if (index === 2) {
      props.changeImage('/head/alien_new.png', { width: '5px', height: '511px', marginLeft: '-5000px' });
    } else if (index === 3) {
      props.changeImage('/head/pepe.png', { width: '600px', height: '600px', marginLeft: '-40px' });
    } 
    else if (index === 4) {
      props.changeImage('/head/chucky.png', { width: '700px', height: '700px', marginLeft: '-60px' });
    }
    else if (index === 5) {
      props.changeImage('/head/joker.png', { width: '800px', height: '800px', marginLeft: '-70px' });
    }
    else if (index === 6) {
      props.changeImage('/head/marshmello.png', { width: '700px', height: '700px', marginLeft: '-60px' });
    }
    else if (index === 7) {
      props.changeImage('/head/deppepe.png', { width: '700px', height: '700px', marginLeft: '-60px' });
    }
    else if (index === 8) {
      props.changeImage('/head/hrhead.png', { width: '700px', height: '700px', marginLeft: '-60px' });
    }
    else if (index === 9) {
      props.changeImage('/head/haloween.png', { width: '700px', height: '700px', marginLeft: '-60px' });
    }
    else if (index === 10) {
      props.changeImage('/head/grim_new.png', { width: '700px', height: '700px', marginLeft: '-60px' });
    }
  };
  const handleClick4 = (index: number) => {
    console.log("Slide clicked:", index);
    if (index === 1) {
      props.changeImage('/head/pinkeyes.png', { width: '400px', height: '400px', marginLeft: '-30px' });
    } 
    else if (index === 0) {
      props.changeImage('/aa.png', { width: '5px', height: '511px', marginLeft: '-5000px' });
    }
    else if (index === 2) {
      props.changeImage('/head/blueeyes.png', { width: '5px', height: '511px', marginLeft: '-5000px' });
    } else if (index === 3) {
      props.changeImage('/head/browneyes.png', { width: '600px', height: '600px', marginLeft: '-40px' });
    } 
    else if (index === 4) {
      props.changeImage('/head/stach.png', { width: '700px', height: '700px', marginLeft: '-60px' });
    }
    else if (index === 5) {
      props.changeImage('/head/redeys.png', { width: '800px', height: '800px', marginLeft: '-70px' });
    }
    else if (index === 6) {
      props.changeImage('/head/stach2c.png', { width: '700px', height: '700px', marginLeft: '-60px' });
    }
    else if (index === 7) {
      props.changeImage('/head/smilec.png', { width: '700px', height: '700px', marginLeft: '-60px' });
    }
    else if (index === 8) {
      props.changeImage('/head/cigarc.png', { width: '700px', height: '700px', marginLeft: '-60px' });
    }
    else if (index === 9) {
      props.changeImage('/head/venomc.png', { width: '700px', height: '700px', marginLeft: '-60px' });
    }
    else if (index === 10) {
      props.changeImage('/head/blushc.png', { width: '700px', height: '700px', marginLeft: '-60px' });
    }
    else if (index === 11) {
      props.changeImage('/head/hackerc.png', { width: '700px', height: '700px', marginLeft: '-60px' });
    }
    else if (index === 12) {
      props.changeImage('/head/smaskc.png', { width: '700px', height: '700px', marginLeft: '-60px' });
    }
    else if (index === 13) {
      props.changeImage('/head/specsc.png', { width: '700px', height: '700px', marginLeft: '-60px' });
    }
    else if (index === 14) {
      props.changeImage('/head/tysonc.png', { width: '700px', height: '700px', marginLeft: '-60px' });
    }
  };
  const handleClick5 = (index: number) => {
    console.log("Slide clicked:", index);
    if (index === 1) {
      props.changeImage('/head/bluet.png', { width: '400px', height: '400px', marginLeft: '-30px' });
    } 
    else if (index === 0) {
      props.changeImage('/aa.png', { width: '5px', height: '511px', marginLeft: '-5000px' });
    }
    else if (index === 2) {
      props.changeImage('/head/ct.png', { width: '5px', height: '511px', marginLeft: '-5000px' });
    } else if (index === 3) {
      props.changeImage('/head/greyt.png', { width: '600px', height: '600px', marginLeft: '-40px' });
    } 
    else if (index === 4) {
      props.changeImage('/head/greent.png', { width: '700px', height: '700px', marginLeft: '-60px' });
    }
    else if (index === 5) {
      props.changeImage('/head/lbt.png', { width: '800px', height: '800px', marginLeft: '-70px' });
    }
    else if (index === 6) {
      props.changeImage('/head/pinkt.png', { width: '700px', height: '700px', marginLeft: '-60px' });
    }
    else if (index === 7) {
      props.changeImage('/head/redt.png', { width: '700px', height: '700px', marginLeft: '-60px' });
    }
    else if (index === 8) {
      props.changeImage('/head/sweater.png', { width: '700px', height: '700px', marginLeft: '-60px' });
    }
    else if (index === 9) {
      props.changeImage('/head/whitet.png', { width: '700px', height: '700px', marginLeft: '-60px' });
    }
    else if (index === 10) {
      props.changeImage('/head/rbt.png', { width: '700px', height: '700px', marginLeft: '-60px' });
    }
    
  };
  

  return (
    <div className='w-[350px] md:w-[100%] mt-[0px] md:mt-[1px] ml-0  md:ml-[-10px] '>
     
      <div className="mt-10">
       
        <p className='text-[#4b3b3a] font-bold  ml-2 mb-2'>MASKS</p>
      
       {/*FIRST ROW*/}
      
        <Slider {...settings}>
          {/* Render the static div for the first slide */}
          <div className="about-slide" onClick={() => handleClick3(0)}>
          <div className="bg-[#4b3b3a] h-[100px] ml-[-17px] md:ml-[10px] mr-[73px] md:w-[100px] w-[100px] text-black rounded-xl border-solid border-[2px] border-[#CEC0AD] cursor-pointer hover:bg-[#fff]">
              <Icon icon="system-uicons:no-sign" className='relative left-[12px] md:left-3' width="82" height="62" style={{color: "#CEC0AD" ,fontWeight:"bold",  marginTop:"20px", marginLeft:"-3px"}} />            </div>
          </div>
          
          {/* Generate the rest of the slides dynamically */}
          {face_data1.map((d,index) => (
            <div key={d.name} className="about-slide" onClick={() => handleClick3(index + 1)}>
   <div className="border-[2px] border-[#4b3b3a] h-[100px] ml-[-15px] md:ml-[10px] md:w-[100px] md:px-[2px] mr-[20px] text-black rounded-xl border-solid  hover:bg-[#fff]">           
        <img src={d.img} alt="" className="w-[180px] md:w-[180px] h-[80px] mt-[6px] ml-[2px] cursor-pointer" />
              </div>
            </div>
          ))}
        </Slider>
      </div>
      <div className="mt-10">
        <p className='text-[#4b3b3a] font-bold  ml-2 mb-2'>FACE</p>
        
        {/*FOURTH ROW*/}
      
        <Slider {...settings}>
          {/* Render the static div for the first slide */}
          <div className="about-slide" onClick={() => handleClick(0)}>
          <div className="bg-[#4b3b3a] h-[100px] ml-[-17px] md:ml-[10px] mr-[73px] md:w-[100px] w-[100px] text-black rounded-xl border-solid border-[2px] border-[#CEC0AD] cursor-pointer hover:bg-[#fff]">
              <Icon icon="system-uicons:no-sign" className='relative left-[12px] md:left-3' width="82" height="62" style={{color: "#CEC0AD" ,fontWeight:"bold",  marginTop:"20px", marginLeft:"-3px"}} />            </div>
          </div>
          
          {/* Generate the rest of the slides dynamically */}
          {eye_data.map((d,index) => (
            <div key={d.name} className="about-slide" onClick={() => handleClick4(index + 1)}>
   <div className="border-[2px] border-[#4b3b3a] h-[100px] ml-[-15px] md:ml-[10px] md:w-[100px] md:px-[2px] mr-[20px] text-black rounded-xl border-solid hover:bg-[#fff]">           
        <img src={d.img} alt="" className="w-[180px] md:w-[180px] h-[80px] mt-[6px] ml-[2px] cursor-pointer" />
              </div>
            </div>
          ))}
        </Slider>
      </div>
   {/* Slider */}
      <div className="mt-10">
        <p className='text-[#4b3b3a] font-bold  ml-2 mb-2'>HAT / HAIR</p>
       
        {/*SECOND ROW*/}
      
        <Slider {...settings}>
         
          <div className="about-slide" onClick={() => handleClick2(0)}>
          <div className="bg-[#4b3b3a] h-[100px] ml-[-17px] md:ml-[10px] mr-[73px] md:w-[100px] w-[100px] text-black rounded-xl border-solid  border-[2px] border-[#CEC0AD] cursor-pointer hover:bg-[#fff]">
              <Icon icon="system-uicons:no-sign" className='relative left-[12px] md:left-3' width="82" height="62" style={{color: "#CEC0AD" ,fontWeight:"bold",  marginTop:"20px", marginLeft:"-3px"}} />            </div>
          </div>
          
       
          {face_data.map((d, index) => (
    <div key={d.name} className="about-slide" onClick={() => handleClick2(index + 1)}>
<div className=" h-[100px] ml-[-15px] md:ml-[10px] md:w-[100px] md:px-[2px] mr-[20px] text-black rounded-xl border-solid border-[2px] border-[#4b3b3a] hover:bg-white">
<img src={d.img} alt="" className="w-[180px] md:w-[180px] h-[80px] mt-[6px] ml-[2px] cursor-pointer" />
        </div>
    </div>
))}

        </Slider>
      </div>
      <div className="mt-10">
        <p className='text-[#4b3b3a] font-bold  ml-2 mb-2'>SHIRT</p>
        
        {/*FIFTH ROW*/}
      
        <Slider {...settings}>
          {/* Render the static div for the first slide */}
          <div className="about-slide" onClick={() => handleClick5(0)}>
          <div className="bg-[#4b3b3a] h-[100px] ml-[-17px] md:ml-[10px] mr-[73px] md:w-[100px] w-[100px] text-black rounded-xl border-solid border-[2px] border-[#CEC0AD] cursor-pointer hover:bg-[#fff]">
              <Icon icon="system-uicons:no-sign" className='relative left-[12px] md:left-3' width="82" height="62" style={{color: "#CEC0AD" ,fontWeight:"bold",  marginTop:"20px", marginLeft:"-3px"}} />            </div>
          </div>
          
          {/* Generate the rest of the slides dynamically */}
          {shirt_data.map((d,index) => (
            <div key={d.name} className="about-slide" onClick={() => handleClick5(index + 1)}>
   <div className="border-[2px] border-[#4b3b3a] h-[100px] ml-[-15px] md:ml-[10px] md:w-[100px] md:px-[2px] mr-[20px] text-black rounded-xl border-solid hover:bg-[#fff]">           
        <img src={d.img} alt="" className="w-[180px] md:w-[180px] h-[80px] mt-[6px] ml-[2px] cursor-pointer" />
              </div>
            </div>
          ))}
        </Slider>
        
      </div>
      <div className="mt-10">
        <p className='text-[#4b3b3a] font-bold  ml-2 mb-2'>RANDOM</p>
        
        {/*THIRD ROW*/}
      
        <Slider {...settings}>
          {/* Render the static div for the first slide */}
          <div className="about-slide" onClick={() => handleClick(0)}>
          <div className="bg-[#4b3b3a] h-[100px] ml-[-17px] md:ml-[10px] mr-[73px] md:w-[100px] w-[100px] text-black rounded-xl border-solid border-[2px] border-[#CEC0AD] cursor-pointer hover:bg-[#fff]">
              <Icon icon="system-uicons:no-sign" className='relative left-[12px] md:left-3' width="82" height="62" style={{color: "#CEC0AD" ,fontWeight:"bold",  marginTop:"20px", marginLeft:"-3px"}} />            </div>
          </div>
          
          {/* Generate the rest of the slides dynamically */}
          {data.map((d,index) => (
            <div key={d.name} className="about-slide" onClick={() => handleClick(index + 1)}>
   <div className="border-[2px] border-[#4b3b3a] h-[100px] ml-[-15px] md:ml-[10px] md:w-[100px] md:px-[2px] mr-[20px] text-black rounded-xl border-solid hover:bg-[#fff]">           
        <img src={d.img} alt="" className="w-[180px] md:w-[180px] h-[80px] mt-[6px] ml-[2px] cursor-pointer" />
              </div>
            </div>
          ))}
        </Slider>
        <img className="hidden md:block w-[80px] h-[80px] md:ml-[38.5rem] md:mt-[-100px] rotate-[-45deg]" src="/paws.png" alt="" />
      </div>

   
     
    </div>
   
    
  );
  
}


const data = [
  {
    name: `John Morgan`,
    img: `/ht.png`,
  },
  {
    name: `Ellie Anderson`,
    img: `/balloon.png`,
    width: '5000px'
  },
  {
    name: `Nia Adebayo`,
    img: `/gp.png`,    
  },
  {
    name: `Rigo Louie`,
    img: `/mfing.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/ch.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/cbat.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/rma.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/wz.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/headphn.png`,    
  },

];

const eye_data = [
  {
    name: `John Morgan`,
    img: `/pinkeye_img.png`,
  },
  {
    name: `Ellie Anderson`,
    img: `/blueeye_img.png`,
    width: '5000px'
  },
  {
    name: `Nia Adebayo`,
    img: `/browneye_img.png`,    
  },
  {
    name: `Rigo Louie`,
    img: `/stach_img.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/redeyes_img.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/stach2.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/smile.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/cigari.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/venom.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/blush.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/hacker.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/smask.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/specs.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/tyson.png`,    
  },

];

const face_data = [
  {
    name: `John Morgan`,
    img: `/head1.png`,
  },
  {
    name: `Ellie Anderson`,
    img: `/head2.png`,
    width: '5000px'
  },
  {
    name: `Mia Williams`,
    img: `/hat4.png`,    
  },
  {
    name: `Nia Adebayo`,
    img: `/head3.png`,    
  },
  {
    name: `Rigo Louie`,
    img: `/bluehair_img.png`,    
  },
  {
    name: `Rigo Louie`,
    img: `/redhair_img.png`,    
  },
  {
    name: `Rigo Louie`,
    img: `/blhair.png`,    
  },
  {
    name: `Rigo Louie`,
    img: `/lbhair.png`,    
  },
  {
    name: `Rigo Louie`,
    img: `/rdhair.png`,    
  },
  {
    name: `Rigo Louie`,
    img: `/pkhair.png`,    
  },
  {
    name: `Rigo Louie`,
    img: `/firered_img.png`,    
  },
  {
    name: `Rigo Louie`,
    img: `/bunny_img.png`,    
  },
  
];
const face_data1 = [
  {
    name: `John Morgan`,
    img: `/face1.png`,
  },
  {
    name: `Ellie Anderson`,
    img: `/face22.png`,
    width: '5000px'
  },
  {
    name: `Nia Adebayo`,
    img: `/face3.png`,    
  },
  {
    name: `Rigo Louie`,
    img: `/face4.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/joker.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/mmelo.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/de_pepe.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/horse.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/haloween_img.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/grim.png`,    
  },

];
const shirt_data = [
  {
    name: `John Morgan`,
    img: `/bwti.png`,
  },
  {
    name: `Ellie Anderson`,
    img: `/cti.png`,
    width: '5000px'
  },
  {
    name: `Nia Adebayo`,
    img: `/gri.png`,    
  },
  {
    name: `Rigo Louie`,
    img: `/gti.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/lbi.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/pi.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/rti.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/sw.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/wti.png`,    
  },
  {
    name: `Mia Williams`,
    img: `/rbi.png`,    
  },

];

export default AboutSection;
