import React from 'react';
import "./text.css"
export const Text = () => {
  return (
    <div className='text-center ml-[10px] md:ml-0'>
      <h1 className="txt_css txt md:text-[28px]  text-[24px] font-bold " style={{ color: '#4b3b3a',  fontFamily: "Bebas Neue, sans-serif" }}>WALTER the coolest dog on Solana</h1>
    </div>
  );
};
